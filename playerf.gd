extends KinematicBody


onready var cam: Camera = $x_rotation/y_rotation/floating_cam
onready var x_rotation_node: Position3D = $x_rotation
onready var y_rotation_node: Position3D = $x_rotation/y_rotation

export var speed = 7
export var sensitivity = 0.2

var x_rotation = 0
var y_rotation = 0


func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


func _input(event):
	if event is InputEventMouseMotion:
		x_rotation += -event.relative.x * sensitivity
		y_rotation -= event.relative.y * sensitivity
		
		x_rotation = wrapf(x_rotation, 0, 360)
		y_rotation = clamp(y_rotation, -90, 90)
		
		x_rotation_node.rotation_degrees.y = x_rotation
		y_rotation_node.rotation_degrees.x = y_rotation

	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()


func _physics_process(_delta):
	if Input.is_action_pressed("forward") \
	or Input.is_action_pressed("backward") \
	or Input.is_action_pressed("left") \
	or Input.is_action_pressed("right") \
	or Input.is_action_pressed("up") \
	or Input.is_action_pressed("down"):

		var direction = Vector3()
		var rotate_value = cam.global_transform.basis.get_euler().y
		
		direction.z = Input.get_action_strength("backward") - Input.get_action_strength("forward")
		direction.x = Input.get_action_strength("right") - Input.get_action_strength("left")
		direction.y = Input.get_action_strength("up") - Input.get_action_strength("down")
		
		direction = direction.rotated(Vector3.UP, rotate_value).normalized()
		
		var _movement = move_and_slide(direction * speed, Vector3.UP)
