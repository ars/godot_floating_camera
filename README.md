# godot_floating_camera

A simple floating camera implementation for Godot, written in GDScript.

## Controls
Forwards: W <br/>
Backwards: S <br/>
Left: A <br/>
Right: D <br/>
Up: Space <br/>
Down: Left Ctrl <br/>

## Credits
The code for movement and camera rotation are modified versions of [Johnny Rouddro](https://github.com/JohnnyRouddro)'s code from their [Godot Third Person Control](https://www.youtube.com/playlist?list=PLqbBeBobXe09NZez_1LLRcT7NQ9NfUCBC) tutorial ([project repository](https://github.com/JohnnyRouddro/Godot_Third_Person_Shooter), licensed MIT) (Thanks, Johnny!)